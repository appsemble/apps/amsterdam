export {};

declare module '@appsemble/sdk' {
  interface Parameters {
    backLabel?: string;
    forwardLabel?: string;
  }
}
