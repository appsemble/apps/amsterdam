declare const file: string;

declare module '*.gif' {
  export default file;
}

declare module '*.svg' {
  export default file;
}
