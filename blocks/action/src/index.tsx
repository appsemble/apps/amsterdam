import { bootstrap } from '@appsemble/sdk';

import { Field } from '../block';
import styles from './index.css';

bootstrap(({ actions, data, parameters: { fields, title } }) => {
  async function onUpdate(event: Event, field: Field): Promise<void> {
    if ((!field.enum || !field.enum.length) && !field.value) {
      return;
    }

    try {
      const updatedResource = {
        ...data,
        [field.name]: field.enum?.length ? (event.target as HTMLSelectElement).value : field.value,
      };
      await actions.onSubmit(updatedResource);
      await actions.onSuccess(updatedResource);
    } catch (error: unknown) {
      await actions.onError(error);
    }
  }

  return (
    <div className={styles.container}>
      <h1 className={styles.title}>{title}</h1>
      {fields.map((field, index) => {
        const { backgroundColor, color, label, name } = field;
        return field?.enum.length ? (
          <div className={`${styles.actionField} ${styles.enumField}`}>
            <label htmlFor={`${name}.${index}`}>{label || ''}</label>
            <div className="select">
              <select
                className={styles.enum}
                id={`${name}.${index}`}
                onchange={(event) => onUpdate(event, field)}
                value={data[name]}
              >
                {field.enum.map((entry) => (
                  <option value={String(entry.value)}>{entry.label || entry.value}</option>
                ))}
              </select>
            </div>
          </div>
        ) : (
          <button
            className={`button ${styles.button} ${styles.actionField}`}
            disabled={Boolean(field.enum?.length)}
            onclick={field.enum?.length ? undefined : (event) => onUpdate(event, field)}
            // eslint-disable-next-line react/forbid-dom-props
            style={{ backgroundColor, color, borderColor: color }}
            type="button"
          >
            {!field.enum && <span className={styles.actionLabel}>{label || ''}</span>}
          </button>
        );
      })}
    </div>
  );
});
