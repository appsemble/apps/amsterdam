import { bootstrap } from '@appsemble/sdk';
import { getDate, getMonth, parseJSON, subMonths } from 'date-fns';

import { LocationData } from '../block';

type Data = Record<string, number | string>;

interface ProcessedData {
  title: string;
  total: number;
  data: Data[];
}

bootstrap(
  ({
    actions,
    events,
    pageParameters,
    parameters: { date = '$created', weight = 'weight' },
    utils,
  }) => {
    function processData(data: Data[]): void {
      const currentDate = Date.now();
      const currentMonth = getMonth(currentDate);
      const currentMonthData = data.filter((d) => getMonth(parseJSON(d[date])) === currentMonth);
      const groupedMonthData = currentMonthData.reduce((acc, d) => {
        const parsedDate = parseJSON(d[date]);
        const day = getDate(parsedDate);

        if (day in acc) {
          acc[day].total += d[weight] as number;
          acc[day].data.push(d);
        } else {
          acc[day] = {
            title: parsedDate.toISOString(),
            total: d[weight] as number,
            data: [d],
          };
        }

        return acc;
      }, {} as Record<number, ProcessedData>);

      const recentMonths: ProcessedData[] = [];

      for (let i = 1; i <= 3; i += 1) {
        const month = subMonths(currentDate, i);
        const targetMonth = getMonth(month);
        const targetMonthData = data.filter((d) => getMonth(parseJSON(d[date])) === targetMonth);
        recentMonths.push({
          title: month.toISOString(),
          total: targetMonthData.reduce((a, b) => a + Number(b[weight]), 0),
          data: targetMonthData,
        });
      }

      events.emit.currentMonth(Object.values(groupedMonthData));
      events.emit.recentMonths(recentMonths);
    }

    async function loadData(d?: { $filter: string }): Promise<void> {
      try {
        const result = await actions.onLoad<Data[]>({ ...pageParameters, ...d });
        processData(result);
        events.emit.data(result);
      } catch {
        events.emit.data(null, 'Failed to load data');
        utils.showMessage('Failed to load data');
      }
    }

    events.on.filter(async (locationData: LocationData) => {
      await loadData({
        // Ternary fallback is to ensure that the resulting response is an empty array.
        $filter: locationData.containers.length
          ? locationData.containers.map((id) => `containerId eq '${id}'`).join(' or ')
          : "_ eq ''",
      });
    });
  },
);
