export {};

declare module '@appsemble/sdk' {
  interface Actions {
    onLoad: never;
  }

  interface Parameters {
    weight?: string;
    date?: string;
  }

  interface EventEmitters {
    data: never;
    currentMonth: never;
    recentMonths: never;
  }

  interface EventListeners {
    filter: never;
  }
}

export interface LocationData {
  containers: string[];
}
