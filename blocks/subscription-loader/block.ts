declare module '@appsemble/sdk' {
  interface Actions {
    onLoad: never;
  }

  interface EventEmitters {
    data: never;
  }

  interface EventListeners {
    subscriptions: never;
  }

  interface Parameters {
    field?: string;
  }
}

export interface ResourceSubscription {
  create: boolean;
  update: boolean;
  delete: boolean;
  subscriptions: Record<
    string,
    {
      create: boolean;
      update: boolean;
    }
  >;
}
