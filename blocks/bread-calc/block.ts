export {};

declare module '@appsemble/sdk' {
  interface EventEmitters {
    data: never;
  }

  interface EventListeners {
    data: never;
  }

  interface Parameters {
    weight: string;
  }
}
