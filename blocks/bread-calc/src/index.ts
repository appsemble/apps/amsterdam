import { bootstrap } from '@appsemble/sdk';

bootstrap(({ data, events, parameters: { weight } }) => {
  function processData(d?: Record<string, number>[]): void {
    try {
      const bread = d
        // Extract the container status weight
        .map((containerStatus) => containerStatus?.[weight])
        // Convert the weight to a number, just to be sure.
        .map(Number)
        // Filter out NaN and Infinity
        .filter(Boolean)
        // Sum
        .reduce((a, b) => a + b, 0);

      const gas = bread * 0.5;

      events.emit.data({
        gas: Math.round(gas),
        compost: Math.round(bread * 0.4),
        co2: Math.round(gas * 0.4),
        heat: Math.round(bread * 0.06),
        bread: Math.round(bread),
      });
    } catch {
      events.emit.data(null, 'Failed to load data');
    }
  }

  if (data != null) {
    processData(Array.isArray(data) ? data : [data]);
  }

  events.on.data(processData);
});
