# Appsemble Amsterdam styling

> Amsterdam specific styling for the Appsemble platform.

To develop blocks, clone appsemble/appsemble>, and add the following to the `workspaces` section of
`package.json`:

```json
    "../amsterdam/blocks/*",
```
