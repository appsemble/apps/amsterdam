import { join } from 'path';

import { configureLogger, handleError } from '@appsemble/node-utils';
import yargs from 'yargs';

function main(): void {
  yargs
    .scriptName('yarn scripts')
    .option('verbose', {
      alias: 'v',
      describe: 'Increase verbosity',
      type: 'count',
    })
    .option('quiet', {
      alias: 'q',
      describe: 'Decrease verbosity',
      type: 'count',
    })
    .middleware([configureLogger])
    .commandDir(join(__dirname, 'commands'), { extensions: ['ts'] })
    .demandCommand(1)
    .fail(handleError)
    .help()
    .parse(process.argv.slice(2));
}

if (require.main === module) {
  main();
}
